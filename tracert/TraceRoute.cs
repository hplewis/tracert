﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Diagnostics;

namespace tracert {

    /*
     * Handle all route tracing functionality.
     */
    class TraceRoute
    {
        IPAddress ip;

        /**
         * Constructor for a new TraceRoute instance.
         * @param ip The IP address to trace.
         */
        public TraceRoute(IPAddress ip)
        {
            this.ip = ip;
        }

        /**
         * Trace the IP address.
         */
        public void Trace()
        {
            Ping ping = new Ping();
            PingOptions options = new PingOptions();

            Stopwatch watch = new Stopwatch();

            // garbage data to fill icmp buffer
            string data = "abcdefghijklmnopqrstuvwxyz789012";
            byte[] buffer = Encoding.ASCII.GetBytes(data);


            PingReply reply = null;
            options.Ttl = 1; // start at 1 hop
            bool atDestination = false; // have we reached the destination?

            // keep pinging consecutive hops until we reach our destination or exceed the hop limit
            while (!atDestination && options.Ttl <= TraceRouteConfig.MAX_HOPS)
            {

                HopData.PrintHopNumber(options.Ttl);

                IPAddress replyAddress = null;

                // Try multiple times per hop (default 3)
                for (int i = 0; i < TraceRouteConfig.PACKETS_PER_HOP; i++)
                {
                    watch.Reset();
                    watch.Start();
                    reply = ping.Send(this.ip.ToString(), TraceRouteConfig.TIMEOUT, buffer, options);
                    watch.Stop();

                    // if we have an expired ttl or reached our destination, we are succeeding
                    if (reply.Status == IPStatus.TtlExpired || reply.Status == IPStatus.Success)
                    {
                        replyAddress = reply.Address;
                        HopData.PrintRoundTripTime(watch.ElapsedMilliseconds);
                    }
                    // if we reached a timeout, that host probably didn't send an error back
                    else if (reply.Status == IPStatus.TimedOut)
                    {
                        HopData.PrintRoundTripTime(null);
                    }

                    // if we got success, we have finished the trace
                    if (reply.Status == IPStatus.Success)
                    {
                        atDestination = true;
                    }
                }

                HopData.PrintHopAddress(replyAddress);

                options.Ttl++; // go to next hop
            }
        }

    }
}