﻿using System;
using System.Net;

namespace tracert
{

    /*
     * Handle output of hop data.
     */
    class HopData
    {

        /**
         * Print out the hop number.
         * @param hopNumber The hop number to print out.
         */
        public static void PrintHopNumber(int hopNumber)
        {
            Console.Write("{0,3}  ", hopNumber);
        }

        /**
         * Print out a single round trip time for a packet.
         * @param rtt The round trip time for the packet. Or null for timeout.
         */
        public static void PrintRoundTripTime(long? rtt)
        {
            string rttOrTimeout = rtt == null ?
                "*  " : rtt + " ms";
            Console.Write("{0,7}  ", rttOrTimeout);
        }

        /**
         * Print out the IP or 'Timeout.' if none given. Makes a new line.
         * @param ip The IP address to print out or null if timeout.
         */
        public static void PrintHopAddress(IPAddress ip)
        {
            if (ip == null)
            {
                Console.WriteLine("{0}", "Request timed out.");
            }
            else
            {
                string ipString = ip.ToString();
                IPHostEntry hostEntry = null;
                try
                {
                    hostEntry = Dns.GetHostEntry(ipString);
                } catch { }
                if (hostEntry != null && hostEntry.HostName != null)
                {
                    Console.WriteLine("{0} [{1}]", hostEntry.HostName, ipString);
                }
                else
                {
                    Console.WriteLine("{0}", ipString);
                }
            }
        }

    }
}