﻿using System;
using System.Net;
using System.Net.Sockets;

namespace tracert
{
    /*
     * Handle local IP availability checks.
     */
    class IPAvailability
    {

        /**
         * Check if the local machine has an address of a specified type.
         * Ignore link-local ipv6 entries.
         * @param family The IP address family type to check against.
         * @return True if it has an address of the type. False otherwise.
         */
        public static bool IsThereLocalIPOfFamily(AddressFamily family)
        {
            // get host entry for local machine
            IPHostEntry localEntry = Dns.GetHostEntry("");

            foreach (IPAddress ip in localEntry.AddressList)
            {
                if (!ip.IsIPv6LinkLocal && ip.AddressFamily == family)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
