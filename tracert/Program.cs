﻿using System;
using System.Net;
using System.Net.Sockets;

namespace tracert
{
    /*
     * Initial class loaded on program start.
     * Main() function is called.
     * Handle top-level functionality of the application.
     */
    class Program
    {
        /**
         * Main entrypoint for tracert.
         */
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Usage();
                return;
            }

            string host = args[0];
            IPAddress ipv4 = null;
            IPAddress ipv6 = null;

            bool haveIPv4 = IPAvailability.IsThereLocalIPOfFamily(AddressFamily.InterNetwork);
            bool haveIPv6 = IPAvailability.IsThereLocalIPOfFamily(AddressFamily.InterNetworkV6);

            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(host);
                foreach (IPAddress ip in hostEntry.AddressList)
                {
                    if (ip.AddressFamily.ToString() == ProtocolFamily.InterNetworkV6.ToString())
                    {
                        ipv6 = ip;
                    }
                    else if (ip.AddressFamily.ToString() == ProtocolFamily.InterNetwork.ToString())
                    {
                        ipv4 = ip;
                    }
                }
            } catch
            {
                Console.WriteLine("Unable to resolve target system name {0}.", host);
            }

            // if we have ipv4 and they have ipv4, do the ipv4 trace
            if (haveIPv4 && ipv4 != null)
            {
                Console.WriteLine();
                Console.WriteLine("Tracing route to {0} [{1}]\nover a maximum of {2} hops:",
                    host, ipv4.ToString(), TraceRouteConfig.MAX_HOPS);
                Console.WriteLine();

                TraceRoute tr4 = new TraceRoute(ipv4);
                try
                {
                    tr4.Trace();
                } catch
                {
                    Console.WriteLine("Encountered an error when tracing {0}", ipv4.ToString());
                }
            }

            // if we have ipv6 and they have ipv6, do the ipv6 trace
            if (haveIPv6 && ipv6 != null)
            {
                Console.WriteLine();
                Console.WriteLine("Tracing route to {0} [{1}]\nover a maximum of {2} hops:",
                    host, ipv6.ToString(), TraceRouteConfig.MAX_HOPS);
                Console.WriteLine();

                TraceRoute tr6 = new TraceRoute(ipv6);
                try
                {
                    tr6.Trace();
                } catch
                {
                    Console.WriteLine("Encountered an error when tracing {0}", ipv6.ToString());
                }
            }

            // if we did at least one trace, say the trace is complete.
            // Windows tracert outputs 'Trace complete.' even when issues are encountered
            // after the trace begun. For example, exceeded the hop limit still results in
            // a 'Trace complete.' message. For this reason, we are outputting the same here.
            if ( (haveIPv4 && ipv4 != null) || (haveIPv6 && ipv6 != null) )
            {
                Console.WriteLine();
                Console.WriteLine("Trace complete.");
            }
        }

        /**
         * Display a usage message to the user.
         */
        private static void Usage()
        {
            Console.WriteLine("Usage: tracert host");
        }
    }
}