﻿
namespace tracert
{

    /*
     * Hold configuration options for the route tracing application.
     */
    class TraceRouteConfig
    {

        // Packet timeout in milliseconds.
        public const int TIMEOUT = 2000;

        // Maximum number of hops we try to get to the destination.
        public const int MAX_HOPS = 30;

        // Number of packets we send per hop.
        public const int PACKETS_PER_HOP = 3;
    }
}
